# frozen_string_literal: true

require_relative '../../../gitlab/styles/common/banned_constants'

module Rubocop
  module Cop
    module Fips
      # Checks for the usage of MD5, which is not FIPS-compliant.
      # It suggests using a FIPS-compliant alternative like SHA256.
      #
      # @example
      #   # bad
      #   OpenSSL::Digest::MD5.hexdigest('foo')
      #   Digest::MD5.hexdigest('foo')
      #
      #   # good
      #   OpenSSL::Digest::SHA256.hexdigest('foo')
      class MD5 < RuboCop::Cop::Base
        include Gitlab::Styles::Common::BannedConstants

        MESSAGE_TEMPLATE = 'MD5 is not FIPS-compliant. Use %{replacement} instead.'

        REPLACEMENTS = {
          'OpenSSL::Digest::MD5' => 'OpenSSL::Digest::SHA256',
          'Digest::MD5' => 'OpenSSL::Digest::SHA256'
        }.freeze

        def initialize(config = nil, options = nil)
          @message_template = MESSAGE_TEMPLATE
          @replacements = REPLACEMENTS
          @autocorrect = false
          super
        end
      end
    end
  end
end
